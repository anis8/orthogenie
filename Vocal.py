import speech_recognition as sr

word = "Bonjour"
letters = []
x = 0
while x < len(word) + 1:
    x = x + 1
    r = sr.Recognizer()
    with sr.Microphone() as source:
        print("Speak Anything :")
        audio = r.listen(source)
        try:
            text = r.recognize_google(audio, language="fr-FR")
            letters.append(text[0].encode("utf-8"))
            print("You said : {}".format(text))
        except:
            print("Sorry could not recognize what you said")

print(letters)
