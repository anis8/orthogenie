#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import random
from game.Player import Player
import threading
from time import sleep
import pyttsx
import speech_recognition as sr
import serial


class Game:
    def __init__(self):
        self.engine = pyttsx.init()
        self.words = []
        self.players = []
        self.ser = serial.Serial('COM5', 9600)

        with open('game/lib/words.json') as json_file:
            self.data = json.load(json_file)

        for i in range(0, len(self.data)):
            self.words.append(self.data[str(i)])

    def start(self, numberPlayers, winPoints, time):
        winner = None
        timer = None
        for i in range(1, numberPlayers):
            name = raw_input("Nom du joueur numéro " + str(i))
            self.players.append(Player(i, name))
        while True:
            if winner != None:
                break

            for i in range(0, len(self.players)):
                player = self.players[i]
                word = random.choice(self.words)

                self.engine.say("Joueur numero " + str(
                    player.number) + " du nom " + player.name + " tiens toi pret, je vais donner ton mot.")

                self.engine.say("Ton mot est")
                self.engine.runAndWait()
                self.engine.say(word)
                self.engine.runAndWait()
                self.engine.say("Tu as " + str(
                    time) + " secondes pour l'ecrire sans faire de fautes, une fois terminer, il faut dire terminer, si tu souhaites passer ton tour, il faut dire passer")
                self.engine.runAndWait()
                timer = threading.Thread(target=self.startPlayerTimer, args=(player, time))
                timer.start()

                letters = []
                x = 0
                ##

                result = raw_input("Veuillez ecrire correctement le mot ici: ")

                if player.timeSeconds <= time:
                    if result == word:
                        self.players[i].points = self.players[i].points + 1
                        self.ser.write(b'R')
                        self.engine.say("Bonne reponse ! Tu gagnes un point, tu as maintenant " + str(
                            self.players[i].points) + " au total.")
                        self.engine.runAndWait()
                        if self.players[i].points >= winPoints:
                            winner = self.players[i]
                            break
                    else:
                        self.ser.write(b'W')
                        self.engine.say("Mauvaise reponse !")
                        self.engine.runAndWait()

                timer.do_run = False
                timer = Nonel
                player.timeSeconds = 0

        print("Felicitations " + winner.name)

    def startPlayerTimer(self, player, seconds):
        t = threading.currentThread()
        while getattr(t, "do_run", True):
            if player.timeSeconds <= seconds:
                if player.timeSeconds != 0:
                    var = 0
                    # print("Temps réstant " + str(player.timeSeconds))
                player.timeSeconds = player.timeSeconds + 1
                sleep(1)
            else:
                if player.timeSeconds != (seconds + 2):
                    print("Fin du temps, veuillez taper entrer pour passer le tour.")
                    player.timeSeconds = seconds + 2
